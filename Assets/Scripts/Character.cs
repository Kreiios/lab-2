﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed;
    public float jumpForce;

    public bool isGrounded;
    public LayerMask isGroundLayer;
    public Transform groundCheck;
    public float groundCheckRadius;
    public bool isFacingLeft;
    public Animator anime;

    public Transform projectileSpawn;
    public float projectileForce;
    public Projectile projectile;

    public AudioClip jumpSFX;
    public AudioClip shootSFX;
    public AudioClip itemSFX;

    public AudioSource audioSource;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        if (!rb)
        {//ali is awesome
            Debug.LogWarning("No Rigidbody 2D found");
        }

        // Controls Speed of Player
        if (speed == 0)
        {
            speed = 1.4f;
            Debug.Log("Your too slow. Default to" + speed);
        }

        // Controls Player Jump Strenght
        if (jumpForce == 0)
        {
            jumpForce = 3.36f;
            Debug.Log("JumpUp. Default to" + jumpForce);
        }

        // Checks to see if player is on the ground to re-enable jump
        if (groundCheckRadius == 0)
        {
            groundCheckRadius = 0.06f;
            Debug.Log("Still Flying." + groundCheckRadius);
        }

        // Sets projectile Speed
        if (projectileForce == 0)
        {
            projectileForce = 2.0f;
            Debug.Log("Your Too Tired." + projectileForce);
        }

        // Check for ground
        if (!groundCheck)
        {
            Debug.LogError("I can do that yet");
        }

        // Animator
        anime = GetComponent<Animator>();

        if (!anime)
        {
            Debug.LogError("Cant Animate that");
        }

        audioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        // Checks if player is on a ground layer
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, isGroundLayer);

        // Defines character movement (Left and right)
        float moveValue = Input.GetAxisRaw("Horizontal");

        // Defines speed and keeps it constant
        rb.velocity = new Vector2(moveValue * speed, rb.velocity.y);


        // Defines Jump
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Debug.Log("Jumping");
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

            // Sound for Jump
            SoundManager.instance.playSingleSound(jumpSFX);
        }


        // Projectile controls
        if (Input.GetButtonDown("Fire1"))
        {

            Debug.LogAssertion("Where?");

            Projectile temp = Instantiate(projectile, projectileSpawn.position, projectileSpawn.rotation) as Projectile;

            Debug.Log("Fireing Muh Arrows");

            playSound(shootSFX);

            GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;

            // Determines which direction the the projectile should be fired
            if (isFacingLeft)
            {
                temp.GetComponent<Projectile>().speed = projectileForce;
            }
            else
            {
                temp.GetComponent<Projectile>().speed = -projectileForce;
            }
        }

        anime.SetFloat("MoveValue", Mathf.Abs(moveValue));
        if (moveValue < 0 && !isFacingLeft)
            flip();
        else if (moveValue > 0 && isFacingLeft)
            flip();

    }

    
    // Flips projectile spawn depending on the direction of Pit
    void flip()
    {
        isFacingLeft = !isFacingLeft;


        Vector3 scaleFactor = transform.localScale;


        scaleFactor.x *= -1;


        transform.localScale = scaleFactor;
    }

    void playSound(AudioClip clip)
    {
        // Which Audio X to play
        audioSource.clip = clip;

        // PLays Audio X
        audioSource.Play();
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if(c.gameObject.tag == "Collectible")
        {
            Destroy(c.gameObject);

            playSound(itemSFX);

            Debug.Log("Collected");
        }
    }
}

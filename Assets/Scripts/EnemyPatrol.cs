﻿using UnityEngine;
using System.Collections;

public class EnemyPatrol : MonoBehaviour
{
    public float speed;
    Rigidbody2D rb;
    public bool isFacingLeft;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();

        if (speed <= 0)
            speed = 0.4f;


        if(!rb)
        {
            rb = gameObject.AddComponent<Rigidbody2D>();
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(isFacingLeft)
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
        }
	}

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "Wall")
        {
            flip();
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {

        if(c.gameObject.tag == "EnemyBarrier")
        {
            flip();
        }
    }

    void flip()
    {
        isFacingLeft = !isFacingLeft;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;
    }


}

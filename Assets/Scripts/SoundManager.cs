﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{

    static SoundManager _instance = null;

    public AudioSource sfxSource;
    public AudioSource musicSource;

	// Use this for initialization
	void Start ()
    {
	    if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            _instance = this;

            DontDestroyOnLoad(this);
        }
	}
	
    public static SoundManager instance
    {
        get { return _instance; }
        set { _instance = value; }
    }

    public void playSingleSound(AudioClip clip)
    {
        sfxSource.clip = clip;

        sfxSource.Play();
    }
}
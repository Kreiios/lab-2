﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    static GameManager _instance = null;

    public GameManager playerPrefab;

    public Text scoreText;
    int _score;

    int _lives;

    void Awake()
    {

        if(instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;

            DontDestroyOnLoad(this);
        }
    }

	// Use this for initialization
	void Start ()
    {
	

	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(SceneManager.GetActiveScene().buildIndex == 0)
            {
                SceneManager.LoadScene(1);
            }
            else if(SceneManager.GetActiveScene().buildIndex == 2)
            {
                SceneManager.LoadScene(0);
            }
        }
	
	}

    public void spawnPlayer(int spawnLocation)
    {
        string spawnName = SceneManager.GetActiveScene().name + "_Spawn" + spawnLocation;

        Transform spawnPoint = GameObject.Find(spawnName).GetComponent<Transform>();

        Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);

    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
        Debug.Log("Start!");
    }


    public static GameManager instance
    {
        get { return _instance; }
        set { _instance = value; }
    }

    public int score
    {
        get
        {
            return _score;
        }
        set
        {
            _score = value;
            if (scoreText)
                scoreText.text = "score: " + score;
        }
    }


    public int live
    {
        get { return _lives; }
        set
        {
            _lives = value;
        }
    }
}

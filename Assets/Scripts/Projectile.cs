﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float speed;
    public float lifeTime;

    // Use this for initialization
    void Start()
    {
        if (speed == 0)
        {
            speed = 5.0f;
        }

        GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;

        if (lifeTime == 0)
        {
            lifeTime = 0.2f;
        }

        Destroy(gameObject, lifeTime);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D dest)
    {
        Debug.Log("Hit " + dest.gameObject.name);

        {
            Destroy(gameObject);
        }
    }

}